#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "utils.h"

#define BUFFER_MAX_SIZE 10000

using namespace std;

int cat(char *filename_beg, list *file_list)
{
	
	char *filename_end = strstr(filename_beg, " ");
	int file_size = 0;
	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

	if(filename_end != NULL)
	{

		filename_end[0] = '\0';

		//cria nova estrutura de arquivo, que contém nome e posições de início e fim do arquivo
		file_t *new_file = initFileStructure();
		strcpy(new_file -> filename, filename_beg);

		//inicio do arquivo
		new_file -> file_beg = file_list -> last -> file_end + 1; 		
		lseek(file_list -> manager_fd, 0, SEEK_END);

		//escrita do texto no arquivo
		filename_beg = &filename_end[1];
		filename_end = strstr(filename_beg, "\\n");	//a quebra de linha nesse gerenciador de arquivos acontece quando se digita "\n", que representa 2 bytes, 
		
		while(filename_end != NULL)
		{
			filename_end[0] = '\n';	//esses dois bytes são substituídos por uma quebra de linha real e o marcador do fim da string.
			filename_end[1] = '\0';
			file_size += write(file_list -> manager_fd, filename_beg, strlen(filename_beg));

			filename_beg = &filename_end[2];

			filename_end = strstr(filename_beg, "\\n");
		}

		//caso no texto não haja nenhuma quebra de linha/ ou apenas para pegar a última linha do texto
		int len = strlen(filename_beg);
		file_size += write(file_list -> manager_fd, filename_beg, len);
		file_size += write(file_list -> manager_fd, "\n", 1);

		//posição do fim do arquivo
		new_file -> file_end = new_file -> file_beg + file_size + 1;	

		//apenas para atualizar os ponteiros nas estruturas dos arquivos	
		char *str = new char[10];
		file_size = strlen(new_file -> filename);

		file_size += sprintf(str, "%d", new_file -> file_beg) + 1;
		file_size += sprintf(str, "%d", new_file -> file_end) + 1;

		if(new_file -> file_beg < 10)
			file_size += 2;

 		delete []str;

		//atualiza o último arquivo da lista
		file_list -> last -> next_file = new_file;
		file_list -> last = new_file;

		return file_size;
	}

	return 0;
}

void more(char *command, file_t *file, list *file_list)
{
	if(file == NULL) 
		return;

	if(!strcmp(command, "file_manager.txt"))
	{
		cout << "Impressão do disco não é permitida. Use o comando \"ls\"\n";
		return;
	}
	if(!strcmp(file -> filename, command))
	{
		
		unsigned size = file -> file_end - file -> file_beg;
		char *text = new char[size + 1];
		lseek(file_list -> manager_fd, file -> file_beg - 1, SEEK_SET);
		read(file_list -> manager_fd, text, size);
		cout << "===\n" << text << "===\n";
		delete []text;
	}
	else
		more(command, file -> next_file, file_list);	
}

void ls(file_t *head)
{
	if(head == NULL)
	{
		cout << "\n";
		return;
	}

	cout << head -> filename << "\t";

	ls(head -> next_file);
}

int main()
{
	char c;		
	char *command = new char[BUFFER_MAX_SIZE];
	file_t *head;
	list *file_list = new list;

	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	file_list -> manager_fd = open("file_manager.txt", O_RDWR | O_CREAT, mode);

	int offset;
	
	head = getFileStructures(0, 1, file_list);

	if(head == NULL)
	{	
		head = new file_t;
		head -> filename = new char[17];
		strcpy(head -> filename, "file_manager.txt");
		head -> file_beg = 0;
		head -> file_end = 0;
		offset = 22;
	}
	else
		offset = 0;	
	
	file_list -> first = head;
	file_list -> last = getLastStructure(head);

	cin.getline(command, BUFFER_MAX_SIZE - 1);
	

	if(!strncmp(command, "cat", 3))
		offset += cat(&command[4], file_list);
	else if(!strncmp(command, "ls", 2))
		ls(head);
	else if(!strncmp(command, "more", 4))
		more(&command[5], head, file_list);
		
	updateManager(file_list, offset);	
		

	close(file_list -> manager_fd);		
	destroyList(&head);

	delete file_list;
	delete []command;

	return 0;
}

