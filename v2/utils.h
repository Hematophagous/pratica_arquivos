#ifndef UTILS_H
#define UTILS_H

typedef struct _file_t
{
	char *filename;
	unsigned file_beg;
	unsigned file_end;
	struct _file_t *next_file;
}file_t;

typedef struct _list{
	file_t *first;
	file_t *last;
}list;

file_t *initFileStructure(const char *filename);

file_t *getSSegment(int manager_fd);

file_t *splitSegment(char *segment);

list *listOfFiles();

file_t *getLastStructure(file_t *head);

void offsetFiles(file_t *file);

void destroyList(file_t **file);

void saveFile(list *file_list, int offset);

void offsetFiles(file_t *file, int offset);

int calcOffset(file_t *file, list *l);

void saveFileStructures(list *l, int manager_fd);
/*
file_t *getFileStructures(unsigned seek_cur, unsigned segment_end, list *l);



void destroyList(file_t **head);

void updateFileStructures(file_t *node, int offset);

void saveFileStructure(file_t *head, list *l);

void updateManager(list *l, unsigned offset);

*/
#endif //UTILS_H
