#include "commands.h"
#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "utils.h"

using namespace std;

int main()
{
	char *command = new char[10000];

	list *file_list = listOfFiles();
	int offset = 0;

	while(true)
	{
		cin.getline(command, 9999);

		if(!strncmp(command, "cat", 3))
		{
			offset += cat(&command[4], file_list);
			if(offset < 0)
			{
				cout << "Digite o nome do arquivo e algum conteúdo\n";
			}
		}
		else if(!strncmp(command, "ls", 2))
			ls(file_list -> first);
		else if(!strncmp(command, "more", 4))
		{
			char *cmd = new char[strlen(&command[5]) + 1];
			strcpy(cmd, &command[5]);
			more(file_list -> first, cmd);
			delete cmd;
		}
		else if(!strncmp(command, "quit", 4))
			break ;
		else	
			cout << "Comando não encontrado\n";
	}
	
	saveFile(file_list, offset);		
	destroyList(&file_list -> first);
	delete file_list;
	delete []command;
	return 0;/**/
}
