#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "utils.h"

using namespace std;

file_t *initFileStructure(const char *filename)
{
	file_t *new_file =  new file_t;

	new_file -> filename = new char[50];

	strcpy(new_file -> filename, filename);

	new_file -> file_beg = 0;
	new_file -> file_end = 0;
	
	new_file -> next_file = NULL;

	return new_file;
}

list *listOfFiles()
{
	int manager_fd = open("file_manager.txt", O_RDONLY);
	list *file_list = new list;
	
	if(lseek(manager_fd, 0, SEEK_END) > 0)
	{
		lseek(manager_fd, 0, SEEK_SET);

		file_list -> first = getSSegment(manager_fd);

		char *segment = new char[file_list -> first -> file_end + 1];
		read(manager_fd, segment, file_list -> first -> file_end);

		file_list -> first -> next_file = splitSegment(segment);
		file_list -> last = getLastStructure(file_list -> first);
		
		close(manager_fd);
		delete []segment;

	}
	else
	{
		file_list -> first = file_list -> last = initFileStructure("file_manager.txt");
	}	

	return file_list;		
}

file_t *getSSegment(int manager_fd)
{
	char *filename = new char[17];
	char c;
	file_t *head = NULL;

	if(read(manager_fd, filename, 16))
	{
		if(!strcmp(filename, "file_manager.txt"))
		{	
			head = initFileStructure(filename);

			read(manager_fd, &c, 1);
			if(c == '~')
				read(manager_fd, &c, 1);

			while(c != '~')
				read(manager_fd, &c, 1);

			read(manager_fd, &c, 1);
			while(c != '~'){
				head -> file_end *= 10;
				head -> file_end += (c - 48);
				read(manager_fd, &c, 1);
			}
		}
	}		
	delete []filename;
	return head;
}

file_t *splitSegment(char *segment)
{
	char *delim = strstr(&segment[1], "~");

	if(delim == NULL)
		return NULL;

	delim[0] = '\0';		

	if(segment[0] == '~')
		segment = &segment[1];

	file_t *new_file = initFileStructure(segment);
	
	segment = strstr(&delim[1], "~");
	segment[0] = '\0';

	new_file -> file_beg = atoi(&delim[1]);
	delim = strstr(&segment[1], "~");

	delim[0] = '\0';

	new_file -> file_end = atoi(&segment[1]);
		
	if(delim != NULL)
		new_file -> next_file = splitSegment(&delim[1]);

	return new_file;
}


void destroyList(file_t **file)
{
	file_t *f = *file;
	
	if(f == NULL)
		return;

	destroyList(&(f -> next_file));
	delete []f -> filename;
	delete (*file);
}

file_t *getLastStructure(file_t *head)
{
	if(head -> next_file == NULL)
		return head;

	return getLastStructure(head -> next_file);		
}

void saveFile(list *file_list, int offset)
{
	if(file_list == NULL)
		return ;

	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	int manager_fd = open("file_manager.txt", O_RDONLY);

	char *disk = new char[file_list -> last -> file_end - file_list -> first -> file_end + 1];
	lseek(manager_fd, file_list -> first -> file_end, SEEK_SET);
	read(manager_fd, disk, file_list -> last -> file_end - file_list -> first -> file_end);
	close(manager_fd);

	offsetFiles(file_list -> first, offset);

	manager_fd = open("file_manager.txt", O_WRONLY, mode);

	saveFileStructures(file_list, manager_fd);
	write(manager_fd, disk, file_list -> last -> file_end - file_list -> first -> file_end);

	close(manager_fd);
	delete []disk;
}

void saveFileStructures(list *l, int manager_fd)
{
	if(l == NULL)
		return ;

	file_t *aux = l -> first;
	char *str = new char[20];
	
	while(aux!= NULL)
	{		

		write(manager_fd, aux -> filename, strlen(aux -> filename));
		write(manager_fd, "~", 1);

		int size = sprintf(str, "%d~", aux -> file_beg);
		write(manager_fd, str, size);

		size = sprintf(str, "%d~", aux -> file_end);
		write(manager_fd, str, size);

		aux = aux -> next_file;
	}
	delete str;
}

void offsetFiles(file_t *file, int offset)
{
	if(file == NULL)
		return ;
	if(file -> file_end == 0)
		offset += 22;

	file -> file_end += offset;
	file_t *aux = file -> next_file;

	while(aux != NULL)
	{
		aux -> file_beg += offset;
		aux -> file_end += offset;

		aux = aux -> next_file;
	}
}

int calcOffset(file_t *file, list *l)
{
	if(file == NULL)
		return 0;
	char *num = new char[20];

	int offset = strlen(file -> filename) + 1;
	
	int add = 2;//l -> first -> file_end == 0 ? 2 : 2;
	int beg = sprintf(num, "%d", file -> file_beg);
	int end = sprintf(num, "%d", file -> file_end);

	if(file -> file_beg < 10)
		offset++;
	if(file -> file_end < 10)
		offset++;
	offset += beg + end + add;
	
	delete []num;
	return offset;
}
