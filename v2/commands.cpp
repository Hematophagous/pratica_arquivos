#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "utils.h"

using namespace std;

int cat(char *information, list *file_list)
{
	int manager_fd, size;
	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	
	if(information == NULL)
		return -1;
	
	char *file_start = strstr(information, " ");
	
	if(file_start == NULL)
		return -1;

	file_start[0] = '\0';

	file_t	*new_file = initFileStructure(information);	
	
	manager_fd = open("file_manager.txt", O_WRONLY | O_CREAT | O_APPEND, mode);
	lseek(manager_fd, -1, SEEK_END);
	
	size = write(manager_fd, &file_start[1], strlen(&file_start[1]));
	
	new_file -> file_beg = file_list -> last -> file_end;
	new_file -> file_end = new_file -> file_beg + size;
	
	close(manager_fd);

	file_list -> last -> next_file = new_file;
	file_list -> last = new_file;
	
	return calcOffset(new_file, file_list);
}

void ls(file_t *file)
{

	if(file == NULL)
	{	cout << "\n";
		return ;
	}

	cout << file -> filename << "\t";
	ls(file -> next_file);
}

void more(file_t *file, char *filename)
{
	if(file == NULL)
	{
		cout << "Arquivo não encontrado\n";
		return ;
	}

	if(strcmp(filename, file -> filename))
	{
		more(file -> next_file, filename);	
		return ;
	}

	int manager_fd = open("file_manager.txt", O_RDONLY);
	lseek(manager_fd, file -> file_beg, SEEK_SET);
	char *information = new char[file -> file_end - file -> file_beg + 1];	
	read(manager_fd, information, file -> file_end - file -> file_beg);
	
	cout << "\n" << information << "\n";

	close(manager_fd);	
	delete []information;
}



