#ifndef UTILS_H
#define UTILS_H

typedef struct _file_t
{
	char *filename;
	unsigned file_beg;
	unsigned file_end;
	struct _file_t *next_file;
}file_t;

typedef struct _list{
	file_t *first;
	int manager_fd;
	file_t *last;
}list;

file_t *initFileStructure();

file_t *getFileStructures(unsigned seek_cur, unsigned segment_end, list *l);

file_t *getLastStructure(file_t *head);

void destroyList(file_t **head);

void updateFileStructures(file_t *node, int offset);

void saveFileStructure(file_t *head, list *l);

void updateManager(list *l, unsigned offset);

#endif //UTILS_H
