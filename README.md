##**Gerenciador de Arquivos**

Um simples gerenciador de arquivos que usa as chamadas do UNIX. O gerenciador é emulado em um único arquivo que contém, em sua parte inicial um segmento de estruturas que guardam algumas informações básicas sobre os arquivos do disco emulado.
Cada estrutura possui o nome do arquivo para o qual ela aponta, dois pseudoponteiros, sendo um para o início do arquivo e outro para o fim, e um ponteiro para o próximo arquivo no disco. Essas informações, no disco, são separadas pelo caractere "~".
Essas estruturas ajudam não só a localizar o arquivo mais rapidamente, poupando o gerenciador de percorrer por toda a sequência de bytes que é, como facilita o uso de alguns comandos que são implementados aqui.
É um sistema simples que armazena os aruivos de forma contígua e possui apenas três comandos para gerenciá-los: cat, ls e more, descritos a seguir:
---

###cat

É o comando mais básico do sistema, sem ele, não há arquivos para serem gerenciados, pois é aqui que o usuário criará um novo arquivo, segundo a sintaxe

```cat _nome_do_arquivo_sem_espaçamento_ texto do arquivo sem quebras de linhas.```

Esse comando não reconhece o símbolo ' ' para o título, mas todo o texto do arquivo que será criado pode ser possuir espaçamento. Entretanto, quebras de linhas não são aceitas no título e no texto, que se limitam, ao todo, a 10000 caracteres.
Cada vez que o comando _cat_ é chamado, uma nova estrutura de ponteiros é criada e adicionada ao final da lista de estruturas, apontando para o byte de início daquele arquivo e seu byte final também. Essa estrutura, posteriormente é gravada junto aos seus predecessores no arquivo que representa nosso disco.
Na prática, o que ela faz é abrir o arquivo do disco apenas para a escrita, no modo de apêndice e escreve no final do disco as informações do arquivo criado.

###ls

É o comando utilizado para listar todos os arquivos do disco. 
Simplesmente percorre uma lista que está armazenada na memória principal e que é obtida a partir da leitura do segmento de estruturas, imprimindo na tela o nome do arquivo para o qual cada estrutura aponta.


###more

Comando utilizado para visualizar o conteúdo de um arquivo informado da seguinte forma:
```_more nome_do_arquivo_```
É importante que apenas o nome do arquivo, sem espaçamentos, seja informado. É o comando mais sensível do gerenciador, pois também depende do modo (uppercase/lowercase) com que o nome do arquivo foi informado para encontrá-lo.
Para fazer isso, o gerenciador percorre a lista de estruturas, comparando nó a nó o nome de cada arquivo armazenado até que encontre aquele que possui o mesmo nome que o informado. Então abre o disco em somente leitura, se desloca para o início do arquivo, (informação contida na estrutura previamente consultada) e carrega cada byte dentro do intervalo inicio~fim pertencente ao arquivo. 
Então imprime os caracteres correspondentes na tela e fecha o disco novamente.
